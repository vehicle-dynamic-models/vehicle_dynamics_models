
class Spring_D:
    def __init__(self):
        self._stiffness = 0

    @property
    def stiffness(self):
        return self._stiffness

    @stiffness.setter
    def stiffness(self, value):
        if value < 0:
            raise ValueError("Value cannot be negative")
        elif value == 0:
            raise ValueError("Value cannot be zero")
        else:
            self._stiffness = value