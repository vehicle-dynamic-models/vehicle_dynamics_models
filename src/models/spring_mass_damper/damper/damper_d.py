

class Damper_D:
    def __init__(self):
        self._damping = 0

    @property
    def damping(self):
        return self._damping

    @damping.setter
    def damping(self, value):
        self._damping = value