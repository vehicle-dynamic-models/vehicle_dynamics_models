from Design.damper_d import Damper_D
from Design.mass_d import Mass_D
from Design.spring_d import Spring_D


class SpringMassDamper_D:
    def __init__(self):
        self.mass = Mass_D()
        self.damper = Damper_D()
        self.spring = Spring_D()

    def initialize_default_values(self):
        self.mass.weight = 100
        self.spring.stiffness = 100
        self.damper.damping = 50