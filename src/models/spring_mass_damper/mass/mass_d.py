

class Mass_D:
    def __init__(self):
        self._weight = 0

    @property
    def weight(self):
        return self._weight

    @weight.setter
    def weight(self, value):
        if value < 0:
            raise ValueError("Value cannot be negative")
        elif value == 0:
            raise ValueError("Value cannot be zero")
        else:
            self._weight = value