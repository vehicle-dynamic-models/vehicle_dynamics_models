from Managers.ProjectFramework import ProjectFramework


class BasicProperties_D(ProjectFramework):
    """

    Creates an instance of the Aerodynamics_D class

    :param weight: car's total weight [N]
    :param wheelbase: The wheel base of the vehicle
    :param suspended_weight_distribution: the suspended weight distribution
    :param suspended_weight_CG: The suspended weight height in the z coordinate [int]
    :param name: Name of the object [str]

    Example::

        obj = BasicProperties_D()
    """
    def __init__(self,
                 weight=0,
                 wheelbase = 0,
                 suspended_weight_distribution=0,
                 suspended_weight_CG=0,
                 name = ""):
        ProjectFramework.__init__(self, name, ".bp", "0.0.0")

        # Todo the word weight needs to be corrected. A better name is weight
        # Todo the weight should be name suspended_weight
        self.weight = weight
        self.wheelbase = wheelbase
        self.suspended_weight_distribution = suspended_weight_distribution
        self.suspended_weight_CG = suspended_weight_CG

    def init_default_values(self):
        """
        Initializes default values for the class

        :return: Nothing
        """
        self.weight = 994 * 9.81
        self.suspended_weight_CG = 0.27
        self.suspended_weight_distribution = 0.47
        self.wheelbase = 2.980

    # region save/load

    # Save object
    def save_object(self, file_path=""):
        """
        Saves this object into a JSON file into the directory specified in file_path

        :param file_path: file_path where the Json file is to be saved. Ex: "Desktop/User/" the name and extension are
        automatically added from the object properties self.name self.extension
        :return: Nothing

        .. warning::

            To save an object the self.name properties cannot be empty.

        Example::

            obj = BasicProperties_D() #Create Class
            obj.init_default_values() #Init default values
            obj.name = "Example"  #A name for the object is necessary if you want to save it
            obj.save_object("C:\\Users\\joaop\\Desktop") #Save

        """

        # The local variable data, is a dictionary where we will be saving all the parameters that we want. Typically
        # this parameters will be the name, version, date, user (from the project Framework class), and the additional
        # parameters that are part of the class itself. After that the function will call the save_to_json function which
        # will save the data into a file.

        data = {
            "name": self.name,
            "version": self.version,
            "date": self.current_date_time(),
            "user": self.get_user(),
            "weight": self.weight,
            "wheelbase": self.wheelbase,
            "suspended_weight_distribution": self.suspended_weight_distribution,
            "suspended_weight_CG": self.suspended_weight_CG
        }

        self.save_to_json(file_path, data)

    # Load Object
    def load_object(self, file_path):
        """
        Loads the values from a JSON file to the object

        :param file: name of the file with extension Ex: filename.ext
        :return: None

        In the following example we are going to load an object from the aerodynamics class.
        In this case it is necessary to first create the object and then issue the load_object function to load the
        values into the class

        Example::

            obj = BasicProperties_D() #Create Class
            obj.load_object("C:\\Users\\joaop\\Desktop\\Example.bp") #Load

        """
        data = self.load_from_json(file_path)

        self.name = data.get("name")
        self.version = data.get("version")
        self.date = data.get("date")
        self.user = data.get("user")
        self.weight = data.get("weight")
        self.wheelbase = data.get("wheelbase")
        self.suspended_weight_distribution = data.get("suspended_weight_distribution")
        self.suspended_weight_CG = data.get("suspended_weight_CG")

    # endregion
