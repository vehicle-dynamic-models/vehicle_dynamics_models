from Managers.ProjectFramework import ProjectFramework


class Spring_D(ProjectFramework):
    """

    Creates an instance of the Spring_D class

    :param stiffness: material stiffness
    :param motion_ratio: the spring motion ratio.
    :param name: Name of the object [str]

    Example::

        obj = Spring_D()
    """
    def __init__(self,
                 stiffness=0,
                 motion_ratio=0,
                 name=""):
        ProjectFramework.__init__(self, name, ".spr", "0.0.0")

        self.stiffness = stiffness
        self.motion_ratio = motion_ratio

    def init_default_values(self):
        """
        Initializes default values for the class

        :return: Nothing
        """
        self.stiffness = 127.1 * 1000
        self.motion_ratio = 0.82

    # region save/load

    # Save object
    def save_object(self, file_path=""):
        """
        Saves this object into a JSON file into the directory specified in file_path

        :param file_path: file_path where the Json file is to be saved. Ex: "Desktop/User/" the name and extension are
        automatically added from the object properties self.name self.extension
        :return: Nothing

        .. warning::

            To save an object the self.name properties cannot be empty.

        Example::

            obj = Spring_D() #Create Class
            obj.init_default_values() #Init default values
            obj.name = "Example"  #A name for the object is necessary if you want to save it
            obj.save_object("C:\\Users\\joaop\\Desktop") #Save

        """

        # The local variable data, is a dictionary where we will be saving all the parameters that we want. Typically
        # this parameters will be the name, version, date, user (from the project Framework class), and the additional
        # parameters that are part of the class itself. After that the function will call the save_to_json function which
        # will save the data into a file.

        data = {
            "name": self.name,
            "version": self.version,
            "date": self.current_date_time(),
            "user": self.get_user(),
            "stiffness": self.stiffness,
            "motion_ratio": self.motion_ratio
        }

        self.save_to_json(file_path, data)  # Call the json method to save the file

    # Load Object
    def load_object(self, file_path):
        """
        Loads the values from a JSON file to the object

        :param file: name of the file with extension Ex: filename.ext
        :return: None

        In the following example we are going to load an object from the aerodynamics class.
        In this case it is necessary to first create the object and then issue the load_object function to load the
        values into the class

        Example::

            obj = Spring_D() #Create Class
            obj.load_object("C:\\Users\\joaop\\Desktop\\Example.spr") #Load

        """

        data = self.load_from_json(file_path)

        # Get the parameters from the json file.
        self.name = data.get("name")
        self.version = data.get("version")
        self.date = data.get("date")
        self.user = data.get("user")
        self.stiffness = data.get("stiffness")
        self.motion_ratio = data.get("motion_ratio")

    # endregion
