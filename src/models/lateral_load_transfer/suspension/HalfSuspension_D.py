from sympy import Point3D

from Design.ARB_D import ARB_D
from Design.QuarterSuspension_D import QuarterSuspension_D
from Managers.ProjectFramework import ProjectFramework


class HalfSuspension_D(ProjectFramework):
    """

    Creates an instance of the HalfSuspension_D class

    :param left_quarter_suspension_D: The left quarter suspension class
    :param right_quarter_suspension_D: The right quarter suspension class
    :param roll_center: The roll center
    :param pitch_center: The pitch center
    :param arb_D: The arb_D class
    :param non_susp_mass_CG: The non suspended weight CG
    :param track: The vehicle track
    :param name: Name of the object [str]

    Example::

        obj = HalfSuspension_D()
    """
    def __init__(self,
                 left_quarter_suspension_D: QuarterSuspension_D = None,
                 right_quarter_suspension_D: QuarterSuspension_D = None,
                 roll_center : Point3D = Point3D(0,0,0),
                 pitch_center : Point3D = Point3D(0,0,0),
                 arb_D: ARB_D = None,
                 non_susp_mass_CG = 0,
                 track=0,
                 name = ""):

        ProjectFramework.__init__(self, name, ".hsus", "0.0.0")

        # When creating the object if no argument for any of the classes was passed then create an empty
        # object
        if left_quarter_suspension_D is None:
            self.left = QuarterSuspension_D()
        else:
            self.left = left_quarter_suspension_D

        if right_quarter_suspension_D is None:
            self.right = QuarterSuspension_D()
        else:
            self.right = right_quarter_suspension_D

        if arb_D is None:
            self.ARB =ARB_D()
        else:
            self.ARB = arb_D

        self.track = track
        self.roll_center = roll_center
        self.pitch_center = pitch_center
        self.non_susp_mass_CG = non_susp_mass_CG

    def init_default_values(self):
        """
        Initializes default values for the class

        :return: Nothing
        """
        self.left.init_default_values()
        self.right.init_default_values()
        self.ARB.init_default_values()

        self.track = 1.640
        self.roll_center = Point3D(0,0,0.05)
        self.pitch_center = Point3D(0.05,0,0.05)
        self.non_susp_mass_CG = 0.025

    # region save/load

    # Save object
    def save_object(self, file_path=""):
        """
        Saves this object into a JSON file into the directory specified in file_path

        :param file_path: file_path where the Json file is to be saved. Ex: "Desktop/User/" the name and extension are
        automatically added from the object properties self.name self.extension
        :return: Nothing

        .. warning::

            To save an object the self.name properties cannot be empty.

        Example::

            obj = HalfSuspension_D() #Create Class
            obj.init_default_values() #Init default values
            obj.name = "Example"  #A name for the object is necessary if you want to save it
            obj.save_object("C:\\Users\\joaop\\Desktop") #Save

        """

        # The local variable data, is a dictionary where we will be saving all the parameters that we want. Typically
        # this parameters will be the name, version, date, user (from the project Framework class), and the additional
        # parameters that are part of the class itself. After that the function will call the save_to_json function which
        # will save the data into a file.

        data = {
            "name": self.name,
            "version": self.version,
            "date": self.current_date_time(),
            "user": self.get_user(),
            "track": self.track,
            "roll_center": [str(self.roll_center.x), str(self.roll_center.y), str(self.roll_center.z)],
            "pitch_center": [str(self.pitch_center.x), str(self.pitch_center.y), str(self.pitch_center.z)],
            "non_susp_mass_CG": self.non_susp_mass_CG,
        }

        self.save_to_json(file_path, data)

    # Load Object
    def load_object(self, file_path):
        """
        Loads the values from a JSON file to the object

        :param file: name of the file with extension Ex: filename.ext
        :return: None

        In the following example we are going to load an object from the aerodynamics class.
        In this case it is necessary to first create the object and then issue the load_object function to load the
        values into the class

        Example::

            obj = HalfSuspension_D() #Create Class
            obj.load_object("C:\\Users\\joaop\\Desktop\\Example.hsus") #Load

        """
        data = self.load_from_json(file_path)

        #Get the parameters from the json file.
        self.name = data.get("name")
        self.version = data.get("version")
        self.date = data.get("date")
        self.user = data.get("user")
        self.track = data.get("track")

        roll_center_array = data.get("roll_center")
        self.roll_center = Point3D(roll_center_array[0],roll_center_array[1],roll_center_array[2])
        pitch_center_array = data.get("pitch_center")
        self.pitch_center = Point3D(pitch_center_array[0],pitch_center_array[1],pitch_center_array[2])
        self.non_susp_mass_CG = data.get("non_susp_mass_CG")

    # endregion
