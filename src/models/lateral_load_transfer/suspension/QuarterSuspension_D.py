from Design.Spring_D import Spring_D
from Design.Tire_D import Tire_D


class QuarterSuspension_D:
    """

    Creates an instance of the QuarterSuspension_D class

    :param spring_D: the Spring_D class
    :param tire_D: the tire_D class
    :param non_suspended_mass: the non suspended weight
    :param non_suspended_mass_CG: the non suspended weight cg
    :param name: Name of the object [str]

    Example::

        obj = QuarterSuspension_D()
    """
    def __init__(self,
                 spring_D: Spring_D() = None,
                 tire_D: Tire_D() = None,
                 non_suspended_mass=0,
                 non_suspended_mass_CG=0):

        # When creating the object if no argument for any of the classes was passed then create an empty
        # object
        if spring_D is None:
            self.spring = Spring_D()
        else:
            self.spring = spring_D

        if tire_D is None:
            self.tire = Tire_D()
        else:
            self.tire = tire_D

        self.non_suspended_mass = non_suspended_mass
        self.non_suspended_mass_CG = non_suspended_mass_CG

    def init_default_values(self):
        """
        Initializes default values for the class

        :return: Nothing
        """
        self.spring.init_default_values()
        self.tire.init_default_values()
        self.non_suspended_mass = 38 * 9.81
        self.non_susp_mass_CG = 0.330
