# VehicleDynamic Models

Template that contains various vehicle dynamic models.

## Getting Started

Clone the project to have access to the models. Feel free to edit them

### Prerequisites

```
ISaveLoad is necessary
```


## Using the project

You can quickly copy the various different types of models available. Perfect to have
a head start and adapt it to your needs

## Authors

* **Joao Antunes** - *Initial work* - [Gitlab](https://gitlab.com/JoaoPedroAntunes)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

